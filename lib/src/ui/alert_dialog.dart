import "package:flutter/material.dart";
import 'package:graphql_app/src/data/query_mutation.dart';
import 'package:graphql_app/src/model/person.dart';
import "package:graphql_flutter/graphql_flutter.dart";

import 'graphql_configuration.dart';


//**************************** CLASS ALERT DIALOG ******************************
class AlertDialogWindow extends StatefulWidget {
  //Variable
  final PersonModel person;
  final bool isAdd;

  //***************************** CONSTRUCT ************************************
  AlertDialogWindow({Key key, this.person, this.isAdd}) : super(key: key);

  //***************************** CALL STATE ***********************************
  @override
  State<StatefulWidget> createState() =>
      _AlertDialogWindow(this.person, this.isAdd);
}

//************************** CLASS STATE ALERT DIALOG **************************
class _AlertDialogWindow extends State<AlertDialogWindow> {
  //Variable
  TextEditingController txtId = TextEditingController();
  TextEditingController txtName = TextEditingController();
  TextEditingController txtLastName = TextEditingController();
  TextEditingController txtAge = TextEditingController();

  GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();
  QueryMutation addMutation = QueryMutation();

  final PersonModel person;
  final bool isAdd;

  //*************************** CONSTRUCT STATE ********************************
  _AlertDialogWindow(this.person, this.isAdd);

  //******************************** STATE INIT ********************************
  @override
  void initState() {
    super.initState();

    if (!this.isAdd) {
      txtId.text = person.getId();
      txtName.text = person.getName();
      txtLastName.text = person.getLastName();
      txtAge.text = person.getAge().toString();
    }
  }

  //****************************** ROOT WIDGETS ********************************
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return AlertDialog(
      
      title: Text(this.isAdd ? "Add" : "Edit or Delete"),

      content: Container(

        child: SingleChildScrollView(

          //::::::::::::::::::::::::::::: DATA ::::::::::::::::::::::::::
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width,
            ),

            child: Stack(
              children: <Widget>[
                Container(
                  child: TextField(
                    maxLength: 5,
                    controller: txtId,
                    enabled: this.isAdd,
                    decoration: InputDecoration(
                      icon: Icon(Icons.perm_identity),
                      labelText: "ID",
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 80.0),
                  child: TextField(
                    maxLength: 40,
                    controller: txtName,
                    decoration: InputDecoration(
                      icon: Icon(Icons.text_format),
                      labelText: "Name",
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 160.0),
                  child: TextField(
                    maxLength: 40,
                    controller: txtLastName,
                    decoration: InputDecoration(
                      icon: Icon(Icons.text_rotate_vertical),
                      labelText: "Last name",
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 240.0),
                  child: TextField(
                    maxLength: 2,
                    controller: txtAge,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Age", icon: Icon(Icons.calendar_today)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),

      //::::::::::::::::::::::::::::::::: ACTION :::::::::::::::::::::::::::::::
      actions: <Widget>[
        FlatButton(
          child: Text("Close"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        !this.isAdd
            ? FlatButton(
          child: Text("Delete"),
          onPressed: () async {
            //CONNECT TO OUR BACKEND
            GraphQLClient _client = graphQLConfiguration.clientToQuery();

            QueryResult result = await _client.mutate(
              MutationOptions(
                document: addMutation.deletePerson(txtId.text),
              ),
            );
            if (!result.hasErrors) Navigator.of(context).pop();
          },
        )
            : null,

        FlatButton(
          child: Text(this.isAdd ? "Add" : "Edit"),
          onPressed: () async {
            if (txtId.text.isNotEmpty &&
                txtName.text.isNotEmpty &&
                txtLastName.text.isNotEmpty &&
                txtAge.text.isNotEmpty) {
              if (this.isAdd) {
                //CONNECT TO OUR BACKEND
                GraphQLClient _client = graphQLConfiguration.clientToQuery();

                QueryResult result = await _client.mutate(
                  MutationOptions(
                    document: addMutation.addPerson(
                      txtId.text,
                      txtName.text,
                      txtLastName.text,
                      int.parse(txtAge.text),
                    ),
                  ),
                );
                if (!result.hasErrors) {
                  txtId.clear();
                  txtName.clear();
                  txtLastName.clear();
                  txtAge.clear();
                  Navigator.of(context).pop();
                }
              } else {
                //CONNECT TO OUR BACKEND
                GraphQLClient _client = graphQLConfiguration.clientToQuery();

                QueryResult result = await _client.mutate(
                  MutationOptions(
                    document: addMutation.editPerson(
                      txtId.text,
                      txtName.text,
                      txtLastName.text,
                      int.parse(txtAge.text),
                    ),
                  ),
                );
                if (!result.hasErrors) {
                  txtId.clear();
                  txtName.clear();
                  txtLastName.clear();
                  txtAge.clear();
                  Navigator.of(context).pop();
                }
              }
            }
          },
        )
      ],
    );
  }
}
//******************************************************************************
