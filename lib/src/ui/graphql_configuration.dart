import "package:flutter/material.dart";
import "package:graphql_flutter/graphql_flutter.dart";


//************************* CLASS GRAPH QL CONFIGURATION ***********************
class GraphQLConfiguration {

  //Variable
  static HttpLink httpLink = HttpLink(
    uri: "https://examplegraphql.herokuapp.com/graphql",
  );

  ValueNotifier<GraphQLClient> client = ValueNotifier(
    GraphQLClient(
      link: httpLink,
      cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
    ),
  );

  //************************** CLIENT TO QUERY *********************************
  GraphQLClient clientToQuery() {

    return GraphQLClient(
      cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
      link: httpLink,
    );
  }
}
//******************************************************************************
