import "package:flutter/material.dart";
import 'package:graphql_app/src/data/query_mutation.dart';
import 'package:graphql_app/src/model/person.dart';
import "package:graphql_flutter/graphql_flutter.dart";

import 'alert_dialog.dart';
import 'graphql_configuration.dart';


//******************************** CLASS PRINCIPAL *****************************
class PrincipalPage extends StatefulWidget {

  //***************************** CALL STATE ***********************************
  @override
  State<StatefulWidget> createState() => _PrincipalPage();
}

//****************************** STATE PRINCIPAL *******************************
class _PrincipalPage extends State<PrincipalPage> {

  List<PersonModel> listPerson = List<PersonModel>();
  GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();

  //****************************** INIT STATE **********************************
  @override
  void initState() {
    super.initState();
    fillList();
  }

  //***************************** MODEL ****************************************
  void fillList() async {
    QueryMutation queryMutation = QueryMutation();
    //CONNECT TO OUR BACKEND
    GraphQLClient _client = graphQLConfiguration.clientToQuery();

    QueryResult result = await _client.query(
      QueryOptions(
        document: queryMutation.getAll(),
      ),
    );
    if (!result.hasErrors) {
      for (var i = 0; i < result.data["persons"].length; i++) {
        setState(() {
          listPerson.add(
            PersonModel(
              result.data["persons"][i]["id"],
              result.data["persons"][i]["name"],
              result.data["persons"][i]["lastName"],
              result.data["persons"][i]["age"],
            ),
          );
        });
      }
    }
  }

  //****************************** METHOD ADD PERSON ***************************
  void _addPerson(context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        AlertDialogWindow alertDialogWindow =
        new AlertDialogWindow(isAdd: true);
        return alertDialogWindow;
      },
    ).whenComplete(() {
      listPerson.clear();
      fillList();
    });
  }

  //******************************* METHOD EDIT ********************************
  void _editDeletePerson(context, PersonModel person) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        AlertDialogWindow alertDialogWindow =
        new AlertDialogWindow(isAdd: false, person: person);
        return alertDialogWindow;
      },
    ).whenComplete(() {
      listPerson.clear();
      fillList();
    });
  }

  //******************************* ROOT WIDGET ********************************
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Example"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_circle_outline),
            onPressed: () => _addPerson(context),
            tooltip: "Insert new person",
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Text(
              "Person",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 30.0),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 50.0),
            child: ListView.builder(
              itemCount: listPerson.length,
              itemBuilder: (context, index) {
                return ListTile(
                  selected: listPerson == null ? false : true,
                  title: Text(
                    "${listPerson[index].getName()}",
                  ),
                  onTap: () {
                    _editDeletePerson(context, listPerson[index]);
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
//******************************************************************************
