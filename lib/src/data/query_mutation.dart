
//****************************** CLASS QUERY MUTATION **************************
class QueryMutation {

  ///*************************** QUERY GRAPH QL ********************************

  //**************************** METHOD ADD ************************************
  String addPerson(String id, String name, String lastName, int age) {
    return """
      mutation{
          addPerson(id: "$id", name: "$name", lastName: "$lastName", age: $age){
            id
            name
            lastName
            age
          }
      }
    """;
  }

  //***************************** METHOD GET ALL *******************************
  String getAll(){
    return """ 
      {
        persons{
          id
          name
          lastName
          age
        }
      }
    """;
  }

  //****************************** METHOD DELETE *******************************
  String deletePerson(id){
    return """
      mutation{
        deletePerson(id: "$id"){
          id
        }
      } 
    """;
  }

  //******************************** METHOD EDIT *******************************
  String editPerson(String id, String name, String lastName, int age){
    return """
      mutation{
          editPerson(id: "$id", name: "$name", lastName: "$lastName", age: $age){
            name
            lastName
          }
      }    
     """;
  }
}
//******************************************************************************
