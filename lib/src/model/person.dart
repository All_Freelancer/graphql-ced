
//****************************** CLASS PERSON MODEL ****************************
class PersonModel {
  //Variable
  final String id;
  final String name;
  final String lastName;
  final int age;

  //******************************** CONSTRUCTOR *******************************
  PersonModel(this.id, this.name, this.lastName, this.age);

  //**************************** METHOD GET ************************************
  getId() => this.id;

  getName() => this.name;

  getLastName() => this.lastName;

  getAge() => this.age;
}
//******************************************************************************
