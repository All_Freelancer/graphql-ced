import 'package:flutter/material.dart';
import 'package:graphql_app/src/ui/graphql_configuration.dart';
import 'package:graphql_app/src/ui/principal.dart';
import "package:graphql_flutter/graphql_flutter.dart";


//*************************** VARIABLE GLOBAL **********************************
GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();


//****************************** CLASS ROOT MAIN *******************************
void main() => runApp(

  GraphQLProvider(
    client: graphQLConfiguration.client,
    child: CacheProvider(
        child: MyApp()
    ),
  ),

);

//***************************** CLASS MY APP ***********************************
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Example',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PrincipalPage(),
    );
  }
}
//******************************************************************************
